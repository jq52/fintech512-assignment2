package assignment2;

class Bottles {
	//public int bot_num;
	public String verse(int verseNumber) {
		int bot_num = verseNumber;
		if (bot_num == 2) {
			bot_num--;
			return (bot_num+1) + " bottles of beer on the wall, " + (bot_num+1) + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (bot_num) + " bottle of beer on the wall.\n";
		}else if (bot_num == 1){
			bot_num--;
			return "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
					+ "no more bottles of beer on the wall.\n";
		}else if (bot_num == 0){
			bot_num = 99;
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}else{
			bot_num--;
			return (bot_num +1) + " bottles of beer on the wall, " + (bot_num +1) + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (bot_num) + " bottles of beer on the wall.\n";
		}
	}
	public String verse(int startVerseNumber, int endVerseNumber) {
		int num = startVerseNumber;
		String res = verse(num);
		num--;
		while(num>=endVerseNumber){
			res = res + "\n" + verse(num);
			num--;
		}
		return res;
	}

	public String song() {
		return verse(99,0);
	}
}
